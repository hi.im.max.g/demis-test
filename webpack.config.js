import webpack      from 'webpack';
import env          from 'gulp-environment';

module.exports = {
    mode: env.current.name,
    output: {
        filename: 'app.js',
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/preset-env']
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "./node_modulesjquery/dist/jquery.min.js",
            jQuery: "./node_modulesjquery/dist/jquery.min.js",
            "window.jQuery": "./node_modulesjquery/dist/jquery.min.js"
        })
    ]
};