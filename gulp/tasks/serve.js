import bs from 'browser-sync';
import { path } from '../task.path';

// Таск для локального сервера
export const Serve = (done) => {
    bs.init({
        server: {
            baseDir: path.pug.dist.dev,
            index: 'index.html'
        },
        ghostMode: true, // Дублирует поведение на всех страницах
        notify: false,
        host: '0.0.0.0',
        port: 9000
    });

    done();
};
