import { src, dest }    from 'gulp';
import { path }         from '../task.path';
import env              from 'gulp-environment';
import browserSync      from 'browser-sync';

// Таск для изображений
export const Fonts = (done) => {
    src(path.fonts.src)
        .pipe(env.if.development(dest(path.fonts.dist.dev)).else(dest(path.fonts.dist.prod)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
};
