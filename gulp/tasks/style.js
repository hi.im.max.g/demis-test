import { src, dest }    from 'gulp';
import { path }         from '../task.path';
import env              from 'gulp-environment';
import bs               from 'browser-sync';
import scss             from 'gulp-sass';
import autoprefixer     from 'gulp-autoprefixer';
import sourcemaps       from 'gulp-sourcemaps';
import cleanCSS         from 'gulp-clean-css';
import concat           from 'gulp-concat';
import plumber          from 'gulp-plumber';
import notify           from 'gulp-notify';

// Сборка основных стилей
export const Style = (done) => {
    src(path.style.src)
        .pipe(env.if.development(plumber({
            errorHandler: notify.onError("Ошибка в стилях: <%= error.message %>")
        })))
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(scss(
            env.if.production({outputStyle: 'compressed'})
        ))
        .pipe(autoprefixer())
        .pipe(cleanCSS({
            level: 2
        }))
        .pipe(concat('style.min.css'))
        .pipe(env.if.development(sourcemaps.write('../maps')))
        .pipe(env.if.development(dest(path.style.dist.dev)).else(dest(path.style.dist.prod)))
        .pipe(bs.reload({stream: true}))
        done();
}
