import { src, dest }    from 'gulp';
import { path }         from '../task.path';
import env              from 'gulp-environment';
import bs               from 'browser-sync';
import webpackStream    from 'webpack-stream';
import webpack          from 'webpack';
import plumber          from 'gulp-plumber';
import notify           from 'gulp-notify';

export const Script = (done) => {
    src(path.script.src)
        .pipe(env.if.development(plumber({
            errorHandler: notify.onError("Ошибка в js: <%= error.message %>")
        })))
        .pipe(webpackStream( {
            config: require('../../webpack.config.js')
        }, webpack ))
        .pipe(env.if.development(dest(path.script.dist.dev)).else(dest(path.script.dist.prod)))
        .pipe(bs.reload({stream: true}))
        done();
};
