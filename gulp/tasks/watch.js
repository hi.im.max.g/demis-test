import { watch, parallel }      from 'gulp';
import { 
    Pug
}                               from './pug';
import { 
    Style
}                               from './style';
import { 
    Script
}                               from './script';
import { Image }                from './image';
import { path }                 from '../task.path';

export const WatchFiles = (done) => {
    watch(path.pug.watch, parallel(Pug));
    watch(path.style.watch, parallel(Style));
    watch(path.script.watch, parallel(Script));
    watch(path.image.watch, parallel(Image));
    done();
}