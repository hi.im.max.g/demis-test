import { src, dest }    from 'gulp';
import { path }         from '../task.path';
import env              from 'gulp-environment';
import imagemin         from 'gulp-imagemin';
import browserSync      from 'browser-sync';

// Таск для изображений
export const Image = (done) => {
    src(path.image.src)
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(env.if.development(dest(path.image.dist.dev)).else(dest(path.image.dist.prod)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
};
