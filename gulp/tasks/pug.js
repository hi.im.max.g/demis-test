import { src, dest }    from 'gulp';
import { path }         from '../task.path';
import env              from 'gulp-environment';
import bs               from 'browser-sync';
import pug              from 'gulp-pug';
import plumber          from 'gulp-plumber';
import notify           from 'gulp-notify';

global.$ = {
    fs: require('fs')
};

export const Pug = (done) => {
    src(path.pug.src)
        .pipe(env.if.development(plumber({
            errorHandler: notify.onError("Ошибка в шаблонизаторе: <%= error.message %>")
        })))
        .pipe(pug({
            pretty: '    '
        }))
        .pipe(env.if.development(dest(path.pug.dist.dev)).else(dest(path.pug.dist.prod)))
        .pipe(bs.reload({stream: true}))
        done();
};
