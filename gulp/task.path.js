export const path = {
    pug: {
        src: [
            './src/views/pages/*.pug'
        ],
        watch: [
            './mock/*.json',
            './src/views/pages/**/*.pug',
            './src/views/pages/*.pug',
            './src/views/layouts/**/*.pug',
            './src/views/sections/**/*.pug',
            './src/views/components/**/*.pug',
            './src/views/components/*.pug',
        ],
        dist: {
            dev: './build',
            prod: './public_html'
        }
    },
    style: {
        src: './src/assets/styles/style.scss',
        srcVendor: './src/assets/styles/vendor.scss',
        watch: [
            './src/assets/styles/*.scss',
            './src/views/components/**/*.scss',
            './src/views/sections/**/*.scss',
        ],
        dist: {
            dev: './build/assets/css/',
            prod: './public_html/assets/css/'
        }
    },
    script: {
        src: [
            './src/views/sections/sidebar/sidebar.js',
            './src/views/sections/card/supoprt.js',
            './src/views/sections/dashboard/dashboard.js'
        ],
        srcVendor: './src/assets/scripts/vendor.js',
        watch: [
            './src/assets/scripts/*.js',
            './src/views/components/**/*.js',
            './src/views/sections/**/*.js',
        ],
        dist: {
            dev: './build/assets/scripts/',
            prod: './public_html/assets/scripts/'
        }
    },
    image: {
        base: './src/assets/img/',
        src: './src/assets/img/**/*.{jpeg,jpg,png}',
        watch: './src/assets/img/**/*.*',
        dist: {
            dev: './build/assets/img/',
            prod: './public_html/assets/img/'
        }
    },
    fonts: {
        src: './src/assets/fonts/**/*.*',
        watch: './src/assets/fonts/**/*.*',
        dist: {
            dev: './build/assets/fonts/',
            prod: './public_html/assets/fonts/'
        }
    }
};