import $ from 'jquery';
import { data } from './data';

export const graphic = function() {
    // Несколько переборов необходимо, чтобы линии не перекрывали друг друга
    
    // Добавляем линии для hover
    $.each(data, function(index, value){
        $('svg').append('<line class="d-none vertical-line" id="verticalLine-'+index+'" x1="'+ value.coords.x1 +'" y1="'+ 0 +'" x2="'+value.coords.x1 +'" y2="'+ 248 +'"></line>');
    });
   
    // Ломанные линии
    $.each(data, function(index, value){
        $('svg').append('<line x1="'+ value.coords.x1 +'" y1="'+ value.coords.y1+'" x2="'+ value.coords.x2+'" y2="'+ value.coords.y2+'"></line>');
    });
    
    // Маркеры и описание
    $.each(data, function(index, value){
        $('svg').append('<circle id="'+index+'"r="6" cx="'+ value.coords.x1 +'" cy="'+value.coords.y1+'"></circle>');
        $('#list').append('<div class="list__item" id="listItem-'+index+'">'+
            '<div class="list__item--left">' + 
                '<div class="item__title">'+ value.title + '</div>' + 
                '<div class="item__text">'+ value.text + '</div>' + 
            '</div>' + 
            '<div class="list__item--right">' +
                '<div class="item__money ' +  value.status + '">'+ '$'+ value.money + '</div>' + 
                '<div class="item__date">'+ value.date + '</div>' + 
            '</div></div>');
    });

    // Описание маркера
    $.each(data, function(index, value){
        if (value.position == 'left') {
            $('svg').append('<div class="d-none descr description-title description-title--left" id="descr-'+index+'" style="left:'+(value.coords.x1-265)+'px; top: '+(value.coords.y1-10)+'px;">'+value.title+'<span class="description-money">'+'$'+value.money+'</span></div>');
        } else {
            $('svg').append('<div class="d-none descr description-title" id="descr-'+index+'" style="left:'+(value.coords.x1+45)+'px; top: '+(value.coords.y1-10)+'px;">'+value.title+'<span class="description-money">'+'$'+value.money+'</span></div>');
        }
    });
    
    $("#graph").html($("#graph").html());

    $('svg circle').mouseover(function() {
        var circleID = $(this).prop('id');
        $('line.vertical-line').addClass('d-none');
        $('.descr').addClass('d-none');
        $('line.vertical-line#verticalLine-'+circleID).addClass('d-block').removeClass('d-none');
        $('.descr#descr-'+circleID).addClass('d-block').removeClass('d-none');
    });

    $('svg circle').mouseout(function() {
        $('line.vertical-line').addClass('d-none').removeClass('d-block');
        $('.descr').addClass('d-none').removeClass('d-block');
    });

    $('.list__header_view-all a.show-list').on('click', function(e) {
        e.preventDefault();
        $('.items__list').css('height', 'auto');
    });
};

graphic();

