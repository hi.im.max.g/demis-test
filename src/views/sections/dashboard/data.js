export const data = [
    {
        'title': 'Green Dot POS Money Load',
        'text': 'Funds loaded through Green Dot load network.',
        'money': '834.19',
        'date': '01/09/13',
        'status': 'up',
        'coords': {
            'x1': 33,
            'y1': 160,
            'x2': 68,
            'y2': 132,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'status': 'down',
        'date': '01/07/13',
        'coords': {
            'x1': 68,
            'y1': 132,
            'x2': 106,
            'y2': 150,
        }
    }, {
        'title': 'Load Money',
        'text': 'Funds Transfered From Bank Acount *5075',
        'money': '188.17',
        'status': 'up',
        'date': '01/05/13',
        'coords': {
            'x1': 106,
            'y1': 150,
            'x2': 149,
            'y2': 126,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'VONS STORE 2511, HENDERSON, NV, USA',
        'money': '7.19',
        'status': 'down',
        'date': '01/09/13',
        'coords': {
            'x1': 149,
            'y1': 126,
            'x2': 192,
            'y2': 192,
        }
    }, {
        'title': 'Load Money',
        'text': 'Funds Transfered From Bank Acount *5075',
        'money': '188.17',
        'status': 'up',
        'date': '01/05/13',
        'coords': {
            'x1': 192,
            'y1': 192,
            'x2': 231,
            'y2': 101,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 231,
            'y1': 101,
            'x2': 267,
            'y2': 143,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 267,
            'y1': 143,
            'x2': 310,
            'y2': 87,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 310,
            'y1': 87,
            'x2': 351,
            'y2': 58,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 351,
            'y1': 58,
            'x2': 389,
            'y2': 104,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 389,
            'y1': 104,
            'x2': 431,
            'y2': 168,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 431,
            'y1': 168,
            'x2': 470,
            'y2': 121,
        }
    }, {
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 470,
            'y1': 121,
            'x2': 508,
            'y2': 75,
        }
    }, {
        'position': 'left',
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 508,
            'y1': 75,
            'x2': 549,
            'y2': 93,
        }
    }, {
        'position': 'left',
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 549,
            'y1': 93,
            'x2': 588,
            'y2': 123,
        }
    }, {
        'position': 'left',
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 588,
            'y1': 123,
            'x2': 630,
            'y2': 103,
        }
    }, {
        'position': 'left',
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 630,
            'y1': 103,
            'x2': 669,
            'y2': 75,
        }
    }, {
        'position': 'left',
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 669,
            'y1': 75,
            'x2': 709,
            'y2': 101,
        }
    }, {
        'position': 'left',
        'title': 'POS Signature Purchase',
        'text': 'SMOKERS WORLD II, LAS VEGAS, NV, USA ',
        'money': '450.39',
        'date': '01/07/13',
        'coords': {
            'x1': 709,
            'y1': 101,
            'x2': 709,
            'y2': 101,
        }
    }
];
