import $ from 'jquery';

export const formSupport = function() {
    var form = $('#customerSupport'),
        errorBlock = $('.error'),
        errorCount = $('.error .information__text');

    $('#customerSupport input, #customerSupport textarea').on('keypress', function() {
        if(this.type !== 'email') {
            var that = this;
            setTimeout(function() {
                var res = /[^а-яА-ЯїЇєЄіІёЁ ]/g.exec(that.value);
                that.value = that.value.replace(res, '');
            }, 0);
        }
    });

    form.find('button[type="submit"]').on('click', function(e) {
        e.preventDefault();
        var invalids = form.find(':invalid'),
            url = form.attr('action');
        if (invalids.length !=0) {
            errorCount.html(invalids.length + ' Error')
            errorBlock.show('400');

            form.find('input, textarea').each(function(){
                $(this).css('border-color', '#ececec');
                if ($(this).is(':invalid')) {
                    $(this).css('border-color', '#c9452b');
                }
            })
        } else {
            $.ajax({
                url: url,
                type: "POST",
                dataType: "html",
                data: form.serialize(),
                success: function(response) {
                    result = $.parseJSON(response);
                    form.html('DONE');
                },
                error: function(response) { // Данные не отправлены
                    form.html('Ошибка. Данные не отправлены. Не настроен action.php');
                }
            });
        }
    });
};

formSupport();