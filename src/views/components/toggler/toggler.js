import $ from 'jquery';

export const toggler = function(toggleElem, blockToggleElem) {
    $(toggleElem).on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('open', 400)
        $(blockToggleElem).slideToggle(400).toggleClass('show')
        $('.mobile-logo').toggleClass('br-b-none')
        $('.app').toggleClass('shadow')
    });
};