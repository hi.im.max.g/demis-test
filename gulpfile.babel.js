import { parallel, series } from 'gulp';
import { Serve } from './gulp/tasks/serve';
import { Pug } from './gulp/tasks/pug';
import { Style } from './gulp/tasks/style';
import { Script } from './gulp/tasks/script';
import { WatchFiles } from './gulp/tasks/watch';
import { Image } from './gulp/tasks/image';
import { Fonts } from './gulp/tasks/fonts';


let RunServer = parallel(
    WatchFiles,
    Serve
);

let Build = series(
    Fonts,
    Image,
    Script,
    Pug,
    Style
);

exports.server = RunServer;
exports.build = Build;
